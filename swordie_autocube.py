import pyautogui
import time
from random import randint

import numpy as np
import os
from mss import mss
import cv2
from PIL import Image
import pytesseract

REGION_X = 591
REGION_Y = 356
WIDTH = 295
HEIGHT = 155

DELAY_BETWEEN_COMMANDS = 0.05
AMOUNT_TO_CUBE = 20000
STUCK_LIMIT = 10

# Display x,y of mouse position | For setting up
# MODE_LOCATE = True
MODE_LOCATE = False
MODE_SUM = True
MODE_EXCLUSION = False

pytesseract.pytesseract.tesseract_cmd = r'C:\Program Files\Tesseract-OCR\tesseract.exe'

def main():
    pyautogui.FAILSAFE = True

    countdownStart()

    i = 0
    stuck_counter = STUCK_LIMIT
    while i < AMOUNT_TO_CUBE:
        i += 1
        print("Checking cube: " + str(i))
        time.sleep(DELAY_BETWEEN_COMMANDS)

        if MODE_LOCATE:
            print(pyautogui.position())
            break
        
        loaded = list(pyautogui.locateAllOnScreen('use_another.png', grayscale=True, region=(590,430, 150, 90)))
        if len(loaded) <= 0:
            # Not loaded, try again
            time.sleep(0.2)
            i -= 1
            stuck_counter -= 1
            if stuck_counter <= 0:
                print("Force exit, cubing UI not found")
                break
            continue
        stuck_counter = STUCK_LIMIT

        if MODE_SUM:
            # print("reading")
            LEGENDARY = countStats('Legendary.png')
            # if LEGENDARY >= 3:
            if LEGENDARY >= 2:
                ALL = countStats('Legendary_ALL.png')
                # ALL = countStats('All_Stats.png')
                STR = countStats('ANY_STR.png')
                DEX = countStats('ANY_DEX.png')
                INT = countStats('ANY_INT.png')
                LUK = countStats('ANY_LUK.png')
                # PER_10 = countStats('any_per_char_level.png')
                # PERCENT = countStats('Percent.png') - ALL
                # USEFUL = PER_10 + PERCENT
                # STR = min(countStats('ANY_STR.png'), USEFUL)
                # DEX = min(countStats('ANY_DEX.png'), USEFUL)
                # INT = min(countStats('ANY_INT.png'), USEFUL)
                # LUK = min(countStats('ANY_LUK.png'), USEFUL)
                # MATT = countStats('Magic_ATT.png')
                # ATT = countStats('ATT.png') - MATT
                # BOSS = countStats('Boss_Damage.png')
                DROP = countStats('Item_Drop_Rate.png')
                # CDMG = countStats('Critical_Damage.png')
                # if ALL+STR+DEX+INT+LUK >= 3:
                #     print("DEBUG ANY found")
                #     break
                # if ALL >= 2:
                #     print("ALL found")
                #     break
                if STR >= 2.5:
                    print("STR found")
                    break
                if DEX >= 2.5:
                    print("DEX found")
                    break
                if INT >= 2.5:
                    print("INT found")
                    break
                if LUK + (ALL * 0.6) >= 2.5:
                    print("LUK found")
                    break
                # if ATT >= 3:
                #     print("ATT found")
                #     break
                # if MATT >= 3:
                #     print("MATT found")
                #     break
                if DROP >= 2.5:
                    print("DROP found")
                    break
                # if CDMG + LUK + (0.6 * (ALL)) >= 2.1:
                #     print("C.Dmg found")
                #     break

        if MODE_EXCLUSION:
            # if hasUndesirableStats('any_per_char_level.png'):
            #     continue
            if hasUndesirableStats('Enables_Skill.png'):
                continue
            if hasUndesirableStats('DEX_per_10.png'):
                continue
            if hasUndesirableStats('any_chance_to.png'):
                continue
            if hasUndesirableStats('Abnormal_Status_Resistance.png'):
                continue
            if hasUndesirableStats('Max_HP.png'):
                continue
            if hasUndesirableStats('Max_MP.png'):
                continue
            if hasUndesirableStats('Critical_Rate.png'):
                continue
            if hasUndesirableStats('STR.png'):
                continue
            if hasUndesirableStats('DEX.png'):
                continue
            if hasUndesirableStats('INT.png'):
                continue
            if hasUndesirableStats('HP_Recover.png'):
                continue
            # if hasUndesirableStats('LUK.png'):
            #     continue
            # if hasUndesirableStats('All_Stats.png'):
            #     continue
            # if hasUndesirableStats('Damage.png'):
            #     continue
            # if hasUndesirableStats('IED.png'):
            #     continue
            print("Found combination with no undesirable stats")
            break

        holdKey("Enter")

    # Dont count last cube
    i -= 1
    print("Done: ~" + str(i) + " Cubes used");

def hasAtLeast(stats, target_count):
    count = countStats(stats)
    if count >= target_count:
        # print(str(len(count)) + " " + stats + " found")
        return True
    return False

def countStats(stats):
    return len(list(pyautogui.locateAllOnScreen(stats, grayscale=True, region=(REGION_X,REGION_Y, WIDTH, HEIGHT))))

def hasUndesirableStats(image):
    pos = pyautogui.locateCenterOnScreen(image, grayscale=True, region=(REGION_X,REGION_Y, WIDTH, HEIGHT))
    if pos is not None:
        holdKey("Enter")
        print (str(pos.x) + " " + str(pos.y) + " " + image)
        # GUIclick(pos.x, pos.y)
        return True
    return False

def countdownStart():
    print("Starting", end="")
    for i in range(0, 5):
        print(".", end="")
        time.sleep(1)
    print("Go")

def holdKey(key, seconds=0.1):
    pyautogui.keyDown(key)
    time.sleep(seconds)
    pyautogui.keyUp(key)

def GUIclick(x, y):
    pyautogui.click(x,y, duration=0.1)

def clearConsole():
    os.system('cls' if os.name == 'nt' else 'clear')

if __name__ == "__main__":
    main()